<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class AutocompleteNotEmptyValidator
 */
class AutocompleteNotEmptyValidator extends ConstraintValidator
{
    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param AutocompleteNotEmpty|Constraint $constraint
     *
     */
    public function validate($value, Constraint $constraint)
    {
        $accessor = new PropertyAccessor();

        if (!$accessor->getValue($value, $constraint->property) &&
            !$accessor->getValue($value, $constraint->idProperty)
        ) {
            if ($this->context instanceof ExecutionContextInterface) {
                $this->context->buildViolation($constraint->message)
                    ->atPath($constraint->property)
                    ->addViolation();
            } else {
                // 2.4 API
                $this->context->addViolationAt(
                    $constraint->property,
                    $constraint->message,
                    array(),
                    null
                );
            }
        }
    }
}
