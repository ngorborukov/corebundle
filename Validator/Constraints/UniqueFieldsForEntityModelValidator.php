<?php
namespace Rup\Bundle\CoreBundle\Validator\Constraints;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntityValidator;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueFieldsForEntityModelValidator
 *
 * @package Rup\Bundle\CoreBundle\Validator\Constraints
 */
class UniqueFieldsForEntityModelValidator extends UniqueEntityValidator
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry);

        $this->entityManager = $registry->getManager();
    }

    /**
     * @param Object     $entityModel
     * @param Constraint $constraint
     */
    public function validate($entityModel, Constraint $constraint)
    {
        if ($constraint->class) {

            $id = $this->getEntityProperty($entityModel, 'id');

            if ($id) {
                $constraint->entity = $this->entityManager->getRepository($constraint->class)->find($id);
            } else {
                $constraint->entity = new $constraint->class;
            }

            if ($this->fillEntityFields($entityModel, $constraint->entity, $constraint->fields)) {
                parent::validate($constraint->entity, $constraint);
            }
        }
    }

    /**
     * Fills entity fields
     *
     * @param Object $entityModel
     * @param Object $entity
     * @param Array  $fields
     *
     * @return bool
     */
    protected function fillEntityFields($entityModel, $entity, $fields)
    {
        $result = true;

        if (is_object($entityModel) && is_object($entity) && count($fields)) {

            foreach ($fields as $fieldName) {
                $propertyValue = $this->getEntityProperty($entityModel, $fieldName);

                if ($propertyValue) {
                    $result .= $this->setEntityProperty($entity, $fieldName, $propertyValue);
                }
            }
        }

        return $result;
    }

    /**
     * @param Object $entity
     * @param string $property
     *
     * @return int|null
     */
    protected function getEntityProperty($entity, $property)
    {
        try {
            $propertyAccessor = PropertyAccess::createPropertyAccessor();

            return $propertyAccessor->getValue($entity, $property);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param Object $entity
     * @param string $property
     * @param mixed  $value
     *
     * @return bool
     */
    protected function setEntityProperty($entity, $property, $value)
    {
        try {
            $propertyAccessor = PropertyAccess::createPropertyAccessor();
            $propertyAccessor->setValue($entity, $property, $value);
        } catch (\Exception $e) {
            return false;
        }
    }
}