<?php
namespace Rup\Bundle\CoreBundle\Model;

/**
 * Interface TitledEntity
 *
 * @package Rup\Bundle\CoreBundle\Model
 */
interface TitledEntity
{
    /**
     * @return integer
     */
    public function getId();

    /**
     * @return integer
     */
    public function getTitle();
}
