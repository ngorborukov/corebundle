<?php
namespace Rup\Bundle\CoreBundle\Model\EntityNumeration\Handler;

use Rup\Bundle\CoreBundle\Model\EntityNumeration\EntityNumerationInterface;
use Rup\Bundle\CoreBundle\Model\EntityNumeration\BaseEntityNumerationRepository;
use Rup\Bundle\CoreBundle\Model\EntityNumeration\EntityNumerationRepositoryInterface;
use Rup\Bundle\CoreBundle\Exception\EntityNumeration\EntityNumberDuplicatedException;
use Rup\Bundle\CoreBundle\Utils\DateTimeUtils;

/**
 * Class BaseEntityNumerationHandler
 *
 * @package Rup\Bundle\CoreBundle\Model\EntityNumeration\Handler
 */
class BaseEntityNumerationHandler
{
    /**
     * @var BaseEntityNumerationRepository
     */
    protected $repository;

    /**
     * @param BaseEntityNumerationRepository $repository
     */
    public function __construct(BaseEntityNumerationRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param EntityNumerationInterface $entity
     * @param EntityNumerationRepositoryInterface $entityRepository
     *
     * @return int
     *
     * @throws EntityNumberDuplicatedException
     */
    public function createYearNumeration(EntityNumerationInterface $entity, EntityNumerationRepositoryInterface $entityRepository)
    {
        $entityNumber = $this->repository->getEntityNumeration($entity);

        $timePeriod = DateTimeUtils::createCurrentYearPeriod();

        if ($entityRepository->findByEntityNumberForTimePeriod($entityNumber, $timePeriod[0], $timePeriod[1])) {
            throw new EntityNumberDuplicatedException("Entity '{$entityRepository->getClassName()}' №{$entityNumber} has already been created this year.");
        }

        return $entityNumber;
    }

    /**
     * @param EntityNumerationInterface $entity
     */
    public function incrementEntityNumeration(EntityNumerationInterface $entity)
    {
        $this->repository->incrementEntityNumeration($entity);
    }
}
