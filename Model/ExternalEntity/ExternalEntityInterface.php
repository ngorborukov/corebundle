<?php
namespace Rup\Bundle\CoreBundle\Model\ExternalEntity;

/**
 * Interface ExternalEntityInterface
 *
 * @package Rup\Bundle\CoreBundle\Model\ExternalEntity
 */
interface ExternalEntityInterface
{
    /**
     * @return int
     */
    public function getExternalId();

    /**
     * @param int $externalId
     */
    public function setExternalId($externalId);
}
