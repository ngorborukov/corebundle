<?php
namespace Rup\Bundle\CoreBundle\Model\ExternalEntity;

/**
 * Trait ExternalEntityTrait
 *
 * @package Rup\Bundle\CoreBundle\Model\ExternalEntity
 */
trait ExternalEntityTrait
{
    /**
     * @var int
     */
    protected $externalId;

    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $externalId
     *
     * @return ExternalEntityInterface
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }
}
