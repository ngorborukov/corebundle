<?php
namespace Rup\Bundle\CoreBundle\Model\Paginator;

use Porpaginas\Doctrine\ORM\ORMQueryResult;
use Porpaginas\Result;

/**
 * Class PaginatorTrait
 *
 * @package Rup\Bundle\CoreBundle\Model\Paginator
 */
trait PaginatorTrait
{
    /**
     * @param string $alias
     *
     * @return \Doctrine\ORM\QueryBuilder|\Rup\Bundle\CoreBundle\Repository\QueryBuilder
     */
    abstract protected function createQueryBuilder($alias);

    /**
     * @return Result
     */
    public function findAllForPaginator()
    {
        $qb = $this->createQueryBuilder('e');

        return new ORMQueryResult($qb);
    }
}
