<?php
namespace Rup\Bundle\CoreBundle\Model\ManageDate;

use Rup\Bundle\CoreBundle\Model\Published\PublishedTrait;

/**
 * Class DateTrait
 *
 * @package Rup\Bundle\CoreBundle\Model\ManageDate
 */
trait DateTrait
{
    use PublishedTrait;

    /**
     * @var int $createDate
     */
    private $createDate;

    /**
     * @var int $publishDate
     */
    private $publishDate;

    /**
     * @var int $cancelPublishDate
     */
    private $cancelPublishDate;

    /**
     * @param int $createDate
     *
     * @return mixed
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param int $publishDate
     *
     * @return mixed
     */
    public function setPublishDate($publishDate)
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getPublishDate()
    {
        return $this->publishDate;
    }

    /**
     * @param int $cancelPublishDate
     *
     * @return mixed
     */
    public function setCancelPublishDate($cancelPublishDate)
    {
        $this->cancelPublishDate = $cancelPublishDate;

        return $this;
    }

    /**
     * @return int
     */
    public function getCancelPublishDate()
    {
        return $this->cancelPublishDate;
    }
}