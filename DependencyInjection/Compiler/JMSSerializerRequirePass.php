<?php
namespace Rup\Bundle\CoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class JMSSerializerRequirePass
 *
 * @package Rup\Bundle\CoreBundle\DependencyInjection\Compiler
 */
class JMSSerializerRequirePass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->has('jms_serializer')) {
            $container->setDefinition(
                'rup_core.object_merger.metadata.simple_jms_serializer_metadata_catcher',
                new Definition('Rup\Bundle\CoreBundle\Services\ObjectMerger\Metadata\JMSSerializerMetadataCatcher', array(
                    new Reference('jms_serializer.metadata_factory')
                ))
            );

            $container->setDefinition(
                'rup_core.object_merger.external_entity_merger',
                new Definition('Rup\Bundle\CoreBundle\Services\ObjectMerger\ExternalEntityMerger', array(
                    new Reference('doctrine.orm.entity_manager'),
                    new Reference('rup_core.object_merger.metadata.simple_jms_serializer_metadata_catcher')
                ))
            );

            $container->setDefinition(
                'rup_core.object_merger.base_merger',
                new Definition('Rup\Bundle\CoreBundle\Services\ObjectMerger\BaseMerger', array(
                    new Reference('doctrine.orm.entity_manager'),
                    new Reference('rup_core.object_merger.metadata.simple_jms_serializer_metadata_catcher')
                ))
            );
        }
    }
}
