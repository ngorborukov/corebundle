<?php
namespace Rup\Bundle\CoreBundle\Utils;

/**
 * Interface ArrayConvertible
 *
 * @package Rup\Bundle\CoreBundle\Utils
 */
interface ArrayConvertible
{
    /**
     * Function converts entity fields to array
     *
     * @return array
     */
    function toArray();
} 