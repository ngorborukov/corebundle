<?php
namespace Rup\Bundle\CoreBundle\Utils;

use Rup\Bundle\CoreBundle\Exception\InvalidArgumentException;

/**
 * Class JsonUtils
 *
 * @package Rup\Bundle\CoreBundle\Utils
 */
class JsonUtils
{
    /**
     * Prepares data to json encoding. In case if data contains objects, class of object should implements ArrayConvertible interface
     *
     * @param mixed $data
     *
     * @return array|null
     *
     * @throws InvalidArgumentException
     */
    public static function prepare($data)
    {
        $result = null;

        if (is_scalar($data)) {
            $result = $data;
        } elseif (is_array($data)) {
            $result = array();
            foreach ($data as $key => $item) {
                $result[$key] = self::prepare($item);
            }
        } elseif (is_object($data)) {
            if ($data instanceof ArrayConvertible) {
                return $data->toArray();
            } else {
                throw new InvalidArgumentException(sprintf('Class %s should implement %s interface', get_class($data), 'ArrayConvertible'));
            }
        }

        return $result;
    }
} 