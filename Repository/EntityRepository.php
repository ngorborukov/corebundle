<?php
namespace Rup\Bundle\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository as BaseEntityRepository;
use Rup\Bundle\CoreBundle\Model\ManageableInterface;
use Rup\Bundle\CoreBundle\Model\ManageableTrait;

/**
 * Class EntityRepository
 *
 * @package Rup\Bundle\CoreBundle\Repository
 */
class EntityRepository extends BaseEntityRepository implements ManageableInterface
{
    use ManageableTrait;

    /**
     * @param string $alias
     *
     * @return QueryBuilder
     */
    public function createQueryBuilder($alias)
    {
        return (new QueryBuilder($this->_em))
            ->select($alias)
            ->from($this->_entityName, $alias);
    }
}