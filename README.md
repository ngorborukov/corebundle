## Installation:

add to composer.json

```json
    "require": {
        "rup/core-bundle": "dev-master"
    }
    ....
    "repositories" : [
      {
        "type" : "vcs",
        "url" : "https://ngorborukov@bitbucket.org/ngorborukov/corebundle.git"
      }
    ],
```

run 
```sh
php composer.phar rup/core-bundle update
```
### Add Bundle to your application kernel

```php
// app/AppKernel.php
public function registerBundles()
{
    return array(
        // ...
        new Rup\Bundle\CoreBundle\RupCoreBundle(),
        // ...
    );
}
```

### Configuration

#### Entity numeration
Use following files for implement numeration service:
```
    Rup\Bundle\CoreBundle\Model\EntityNumeration\BaseEntityNumeration
    Rup\Bundle\CoreBundle\Model\EntityNumeration\BaseEntityNumerationRepository
    Rup\Bundle\CoreBundle\Model\EntityNumeration\Handler\BaseEntityNumerationHandler
```
reset-numeration command:
```
    Rup\Bundle\CoreBundle\Command\AbstractResetEntityNumerationCommand
```

### Development

1. At your project navigate to /project/home/folder/.../vendor/rup/core-bundle/Rup/Bundle
2. Remove CoreBundle dir
3. Create link to clonned directory
```sh
ln -s /path/to/clonned/CoreBundle/ CoreBundle
```