<?php
namespace Rup\Bundle\CoreBundle\Dto;

/**
 * Interface CreateDtoInterface
 *
 * @package Rup\Bundle\CoreBundle\Dto
 */
interface CreateDtoInterface
{
    /**
     * @param object $entity
     *
     * @return object
     */
    public function createDto($entity);
} 