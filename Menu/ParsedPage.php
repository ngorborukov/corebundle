<?php
namespace Rup\Bundle\CoreBundle\Menu;

/**
 * Class ParsedPage
 */
class ParsedPage
{
    /**
     * @var boolean
     */
    public $available = true;

    /**
     * @var string
     */
    public $icon;

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $label;

    /**
     * @var integer
     */
    public $level;

    /**
     * @var string
     */
    public $route = '#';

    /**
     * @var array
     */
    public $routeParams;

    /**
     * @var string
     */
    public $resource;

    /**
     * @var string
     */
    public $permission;

    /**
     * @var string
     */
    public $strategy;

    /**
     * @var ParsedPage[]
     */
    public $children;

    /**
     * @param string $key
     */
    public function __construct($key = null)
    {
        $this->key = $key;
    }

    /**
     * @param array $routeParams
     *
     * @return self
     */
    public function setRouteParams($routeParams)
    {
        $this->routeParams = $routeParams;

        return $this;
    }
}
