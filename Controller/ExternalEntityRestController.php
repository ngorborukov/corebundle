<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityInterface;
use Rup\Bundle\CoreBundle\Model\ExternalEntity\ExternalEntityRepositoryInterface;
use Rup\Bundle\CoreBundle\Repository\EntityRepository;
use Rup\Bundle\CoreBundle\Services\ObjectMerger\ObjectMergerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class ExternalEntityRestController extends AbstractRestController
{
    /**
     * @return ObjectMergerInterface
     */
    protected function getEntityMerger()
    {
        return $this->get('rup_core.object_merger.external_entity_merger');
    }

    /**
     * @param $entity
     * @param $mixin
     */
    protected function merge($entity, $mixin)
    {
        $this->getEntityMerger()->merge($entity, $mixin, $this->getDeserializationContext());
    }

    /**
     * @inheritdoc
     */
    protected function findEntity($request)
    {
        /** @var EntityRepository|ExternalEntityRepositoryInterface $repository */
        $repository = $this->getRepository();

        if ($request instanceof Request) {
            $id = $request->get('id');
        } else if ($request instanceof ExternalEntityInterface) {
            return $repository->findOneByExternalId($request->getExternalId());
        } else if (method_exists($request, 'getId')) {
            $id = $request->getId();
        } else {
            $id = (int) $request;
        }

        return $repository->find($id);
    }
}
