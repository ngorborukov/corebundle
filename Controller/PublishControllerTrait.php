<?php
namespace Rup\Bundle\CoreBundle\Controller;

use Rup\Bundle\CoreBundle\Handler\PublishHandlerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class PublishControllerTrait
 *
 * @package Rup\Bundle\CoreBundle\Controller
 */
trait PublishControllerTrait
{
    /**
     * @return PublishHandlerInterface
     */
    abstract protected function getPublishHandler();

    /**
     * @param int $id
     *
     * @return null|object
     *
     * @throws NotFoundHttpException
     */
    abstract protected function findEntity($id);

    /**
     * @param object $entity
     *
     * @return RedirectResponse
     */
    abstract protected function afterEdit($entity);

    /**
     * @return EntityManager
     */
    abstract protected function getManager();

    /**
     * @param  int $id
     *
     * @return RedirectResponse|Response
     */
    public function publishAction($id)
    {
        $entity = $this->findEntity($id);

        if ($this->getPublishHandler()->publish($entity)) {
            $this->getManager()->flush();
        }

        return $this->afterEdit($entity);
    }

    /**
     * @param  int $id
     *
     * @return RedirectResponse|Response
     */
    public function cancelPublishAction($id)
    {
        $entity = $this->findEntity($id);

        if ($this->getPublishHandler()->cancelPublish($entity)) {
            $this->getManager()->flush();
        }

        return $this->afterEdit($entity);
    }
} 