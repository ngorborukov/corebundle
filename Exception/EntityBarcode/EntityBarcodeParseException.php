<?php
namespace Rup\Bundle\CoreBundle\Exception\EntityBarcode;

/**
 * Class EntityBarcodeParseException
 *
 * @package Rup\Bundle\CoreBundle\Exception\EntityBarcode
 */
class EntityBarcodeParseException extends EntityBarcodeException
{

} 