<?php
namespace Rup\Bundle\CoreBundle\Exception\Workflow;

/**
 * Class WorkflowActionException
 *
 * @package Rup\Bundle\CoreBundle\Exception\Workflow
 */
class WorkflowActionException extends WorkflowException
{

} 