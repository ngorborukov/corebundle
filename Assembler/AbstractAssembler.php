<?php
namespace Rup\Bundle\CoreBundle\Assembler;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Class AbstractAssembler
 *
 * @package Rup\Bundle\CoreBundle\Assembler
 */
abstract class AbstractAssembler
{
    /**
     * @param Collection|object[] $objectArray
     *
     * @return array|object[]
     */
    public function createDtoList($objectArray)
    {
        $objectDtoArray = array();

        foreach ($objectArray as $object) {
            $objectDtoArray[] = $this->createDto($object);
        }

        return $objectDtoArray;
    }

    /**
     * @param array|object[] $objectDtoArray
     *
     * @return Collection|object[]
     */
    public function createDomainObjectList($objectDtoArray)
    {
        $objectArray = new ArrayCollection();

        foreach ($objectDtoArray as $objectDto) {
            $object = $this->getRepository()->find($objectDto->id);

            if (!$objectArray->contains($object)) {
                $objectArray->add($object);
            }
        }

        return $objectArray;
    }
}