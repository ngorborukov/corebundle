<?php
namespace Rup\Bundle\CoreBundle\Util\Type;

use Rup\Bundle\CoreBundle\Exception\Type\TypeException;

/**
 * Class AbstractStatusType
 *
 * @package Rup\Bundle\CoreBundle\Util\Type
 */
abstract class AbstractStatusType implements StatusTypeInterface
{
    /**
     * @param mixed $key
     *
     * @return mixed
     *
     * @throws TypeException
     */
    public static function getTypeByKey($key)
    {
        $types = static::getTypes();

        if (!array_key_exists($key, $types)) {
            throw new TypeException(sprintf('Key "%s" was not found in "%s" class.', $key, get_called_class()));
        }

        return $types[$key];
    }

    /**
     * @param mixed $type
     *
     * @return mixed
     *
     * @throws TypeException
     */
    public static function getKeyByType($type)
    {
        if (!$key = array_search($type, static::getTypes())) {
            throw new TypeException(sprintf('Type "%s" was not found in "%s" class.', $type, get_called_class()));
        }

        return $key;
    }
}
