<?php
namespace Rup\Bundle\CoreBundle\Util\Type;

use Rup\Bundle\CoreBundle\Exception\Type\TypeException;

/**
 * Interface StatusTypeInterface
 *
 * @package Rup\Bundle\CoreBundle\Util\Type
 */
interface StatusTypeInterface
{
    /**
     * Must returns array of types
     *
     * @return array
     */
    public static function getTypes();

    /**
     * @param mixed $key
     *
     * @return mixed
     *
     * @throws TypeException
     */
    public static function getTypeByKey($key);

    /**
     * @param mixed $type
     *
     * @return mixed
     *
     * @throws TypeException
     */
    public static function getKeyByType($type);
}
