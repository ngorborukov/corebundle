function ParentPredefinedValueSelector(parentSelector, valueInputSelector, getValueUrl)
{
    var self = this;
    this.$parentElement = $(parentSelector);
    this.$valueInputElement = $(valueInputSelector);
    this.getValueUrl = getValueUrl;

    this.$parentElement.on('change', function(e) {
        if ($(this).val() > 0) {
            self.fillValue(e);
        } else {
            self.$valueInputElement.val(null);
        }
    });
}

ParentPredefinedValueSelector.prototype = {
    fillValue: function(event) {
        var self = this;
        if (self.$parentElement.val() > 0) {
            $.getJSON(
                self.getValueUrl,
                {id: self.$parentElement.val()},
                function(json) {
                    //var json = $.parseJSON(data);

                    if (json.status == 'success') {
                        self.$valueInputElement.val(json.data);
                    }
                }
            );
        }
    }
};
