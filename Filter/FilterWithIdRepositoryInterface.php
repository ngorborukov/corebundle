<?php
namespace Rup\Bundle\CoreBundle\Filter;

use Porpaginas\Result;

/**
 * Interface FilterWithIdRepositoryInterface
 *
 * @package Rup\Bundle\CoreBundle\Filter
 */
interface FilterWithIdRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return Result
     */
    public function findByFilterId($id);
}
