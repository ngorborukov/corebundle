<?php
namespace Rup\Bundle\CoreBundle\Form\DataTransformer\Entity;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class EntityToEntityIdTransformer
 */
class EntityToEntityIdTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var string
     */
    protected $class;

    /**
     *
     */
    public function __construct($em, $class)
    {
        $this->em = $em;
        $this->class = $class;
    }

    /**
     * Transforms an entity id into an entity.
     *
     * @param mixed $entityId
     *
     * @return entity
     */
    public function reverseTransform($entityId)
    {
        if ($entityId){
            $entity = $this->em->getRepository($this->class)->find($entityId);

            return $entity;
        }

        return null;
    }

    /**
     * Transforms entity into entity id.
     *
     * @param mixed $entity
     *
     * @return integer
     */
    public function transform($entity)
    {
        if ($entity instanceof $this->class) {

            return $entity->getId();
        }

        return null;
    }
}
